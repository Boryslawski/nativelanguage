package Util;

import Database.Entity.BiGram;
import Service.BiGramService;

import java.util.*;

public class BigramCorrection {

    public static Boolean print_flag = Boolean.TRUE;
    private BiGramService biGramService = new BiGramService();
    private static ArrayList<String> corrected_bigrams = new ArrayList<>();
    private String[] sentence_words;

    public BigramCorrection(String[] sentence_words){
        this.sentence_words = sentence_words;
    }

    public ArrayList<String> run(){
        corrected_bigrams.clear();

        if (print_flag){
            System.out.println("\n\n========= Bigrams =========");
        }

        ArrayList<String> bigrams_list = new ArrayList<>();

        for(int i=0; i<sentence_words.length; i++){
            if(i!=(sentence_words.length-1)){
                String bigram = sentence_words[i]+" "+sentence_words[i+1];
                bigrams_list.add(bigram);
            }
        }

        for(String s : bigrams_list){

            if(IsInCorpusBigrams(s)){
                String s1[] = s.split(" ");
                corrected_bigrams.add(s1[0]);
                corrected_bigrams.add(s1[1]);
                continue;
            }
            else {
                HashMap<Integer, ArrayList<String>> fixed_bigrams = biGramService.getSimilarBigrams(s, 2);

                if (fixed_bigrams.isEmpty()){
                    String s1[] = s.split(" ");
                    corrected_bigrams.add(s1[0]);
                    corrected_bigrams.add(s1[1]);
                    continue;
                }
                else {
                    Map.Entry<Integer,ArrayList<String>> entry = fixed_bigrams.entrySet().iterator().next();
                    String first_bigram_with_number = entry.getValue().get(0);
                    String[] bigram_parts = first_bigram_with_number.split(":");

                    String s1[] = s.split(" ");
                    String s2[] = bigram_parts[1].split(" ");

                    if (!s1[0].equals(s2[0]))
                        s2[0] = s2[0]+"+";
                    
                    if (!s1[1].equals(s2[1]))
                        s2[1] = s2[1]+"+";

                    corrected_bigrams.add(s2[0]);
                    corrected_bigrams.add(s2[1]);
                }
            }
        }
        System.out.println("Poprawa: "+print_bigrams_result());
        return corrected_bigrams;
    }

    private Boolean IsInCorpusBigrams(String bigram){
        for(BiGram b : biGramService.getBigramsPart()){
            String bigram_ = b.getFirstWord()+" "+b.getSecondWord();
            if(bigram_.equals(bigram)){
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    private String print_bigrams_result(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(corrected_bigrams.get(0).replaceAll("\\+","")+" ");

        for (int i=1; i<corrected_bigrams.size(); i+=2){

            String w1 = corrected_bigrams.get(i);
            if (i+1==corrected_bigrams.size()){
                stringBuilder.append(corrected_bigrams.get(i).replaceAll("\\+","")+" ");
                continue;
            }
            String w2 = corrected_bigrams.get(i+1);

            if(w1.charAt(w1.length()-1)=='+' && w2.charAt(w2.length()-1)=='+'){
                if(w1.equals(w2)){
                    w1 = w1.replaceAll("\\+","");
                    stringBuilder.append(w1+" ");
                }
                else {
                    w1 = w1.replaceAll("\\+","");
                    w2 = w2.replaceAll("\\+","");
                    stringBuilder.append(w1+" ("+w2+") ");
                }
            }

            else if(w2.charAt(w2.length()-1)=='+'){
                w2 = w2.replaceAll("\\+","");
                stringBuilder.append(w2+" ");
            }

            else if(w1.charAt(w1.length()-1)=='+'){
                w1 = w1.replaceAll("\\+","");
                stringBuilder.append(w1+" ");
            }

            else {
                stringBuilder.append(w1+" ");
            }
        }

        stringBuilder.setCharAt(0, Character.toUpperCase(stringBuilder.charAt(0)));

        if (stringBuilder.charAt(stringBuilder.length()-2)!='.'){
            stringBuilder.setCharAt(stringBuilder.length()-1,'.');
        }


        return String.valueOf(stringBuilder);
    }


//    private String print_bigrams_result(ArrayList<String> corrected_text_bigram){
//        StringBuilder stringBuilder = new StringBuilder();
//        Set<String> linkedHashSet = new LinkedHashSet<>();
//
//        for (String s : corrected_text_bigram){
//            String[] split_words = s.split(" ");
//            for(String word : split_words){
//                linkedHashSet.add(word);
//            }
//        }
//
//        for (String s : linkedHashSet){
//            stringBuilder.append(s+" ");
//        }
//
//        stringBuilder.setCharAt(0, Character.toUpperCase(stringBuilder.charAt(0)));
//
//        return String.valueOf(stringBuilder);
//    }

    private static void printSimilarWords(HashMap<Integer, ArrayList<String>> list) {
        System.out.println("Proponowane wyrazy:");
        list.forEach((integer, strings) -> {
            System.out.println(" >> Poziom filtrowania (" + strings.size() + "): " + integer);
            strings.forEach(x -> System.out.println("   " + x));
        });
    }

}
