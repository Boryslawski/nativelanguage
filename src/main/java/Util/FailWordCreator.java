package Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class FailWordCreator {

    private HashMap<String, String> map = new HashMap<>();

    public FailWordCreator(){
        fillFailMap();
    }

    public String create(String sentence, int percentOfFail){

        if (percentOfFail > 100)
            percentOfFail = 100;
        else if (percentOfFail < 0)
            percentOfFail = 0;


        String[] signs = sentence.split("");
        ArrayList<Integer> indexesWhereCanDoFail = getIndexesWhereCanDoFail(sentence);

        float numberOfFailSigns = indexesWhereCanDoFail.size() * percentOfFail / 100f;

        if (numberOfFailSigns < 1)
            numberOfFailSigns = 1;

        numberOfFailSigns = Math.round(numberOfFailSigns);

        ArrayList<Integer> failSignIndexes = new ArrayList<>();
        for(int i = 0; i < numberOfFailSigns; i++){
            int failSignIndex;
            do{
                failSignIndex = indexesWhereCanDoFail.get(new Random().nextInt(indexesWhereCanDoFail.size()));
            }while(failSignIndexes.contains(failSignIndex));

            failSignIndexes.add(failSignIndex);

            String signToChange = signs[failSignIndex];
            signs[failSignIndex] = changeSign(signToChange);
        }

        return String.join("", signs);
    }

    private String changeSign(String sign){

        return map.getOrDefault(sign, sign);
    }

    private void fillFailMap(){
        map.put("u", "ó");
        map.put("ó", "u");
        map.put("w", "f");
        map.put("f", "w");
        map.put("i", "j");
        map.put("j", "i");
        map.put("k", "g");
        map.put("g", "k");
        map.put("d", "t");
        map.put("t", "d");
        map.put("ę", "e");
        map.put("e", "ę");
        map.put("ą", "a");
        map.put("a", "ą");
    }

    private ArrayList<Integer> getIndexesWhereCanDoFail(String sentence){

        String[] signs = sentence.split("");
        ArrayList<Integer> list = new ArrayList<>();

        for(int i = 1; i < signs.length; i++){
            if(map.containsKey(signs[i]))
                list.add(i);
        }

        return list;
    }
}
