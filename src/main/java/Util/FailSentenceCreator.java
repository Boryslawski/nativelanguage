package Util;

import Service.SjoService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class FailSentenceCreator {

    private LevenshteinAlgorithm levenshteinAlgorithm = new LevenshteinAlgorithm();
    private final SjoService sjoService = new SjoService();



    public String create(String sentence, int percentOfFail, int levensteinLevel){

        if (percentOfFail > 100)
            percentOfFail = 100;
        else if (percentOfFail < 0)
            percentOfFail = 0;

        String[] words = sentence.split(" ");
        int sentenceLength = words.length;

        float numberOfFailWords = sentenceLength * percentOfFail / 100f;

        if (numberOfFailWords < 1)
            numberOfFailWords = 1;

        numberOfFailWords = Math.round(numberOfFailWords);

        ArrayList<Integer> failWordIndexes = new ArrayList<>();
        for (int i  = 0; i < numberOfFailWords; i++) {
            int failWordIndex;
            do{
                failWordIndex = new Random().nextInt(sentenceLength);
            }while(failWordIndexes.contains(failWordIndex));

            failWordIndexes.add(failWordIndex);

            String wordToFail = words[failWordIndex];

            // find with levensthein Algorithm sample word for wordToFail
            HashMap<Integer, ArrayList<String>> similar_words = sjoService.getSimilarWords(wordToFail, levensteinLevel);

            if(!similar_words.isEmpty()){
                words[failWordIndex] = similar_words.get(levensteinLevel).get(new Random().nextInt(similar_words.get(levensteinLevel).size()));
            }
        }

        return String.join(" ", words);
    }
}
