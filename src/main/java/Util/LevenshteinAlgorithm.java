package Util;

public class LevenshteinAlgorithm {

    public LevenshteinAlgorithm() {
    }

    public int levenshteinDistance (String lhs0, String rhs0) {
        String lhs1 = lhs0.toLowerCase();
        String rhs1 = rhs0.toLowerCase();
        int len0 = lhs1.length() + 1;
        int len1 = rhs1.length() + 1;

        int[] cost = new int[len0];
        int[] newcost = new int[len0];

        for (int i = 0; i < len0; i++) cost[i] = i;

        for (int j = 1; j < len1; j++) {
            newcost[0] = j;

            for(int i = 1; i < len0; i++) {
                int match = (lhs1.charAt(i - 1) == rhs1.charAt(j - 1)) ? 0 : 1;

                int cost_replace = cost[i - 1] + match;
                int cost_insert  = cost[i] + 1;
                int cost_delete  = newcost[i - 1] + 1;

                newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
            }

            int[] swap = cost; cost = newcost; newcost = swap;
        }

        return cost[len0 - 1];
    }
}
