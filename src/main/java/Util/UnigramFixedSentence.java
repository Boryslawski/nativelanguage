package Util;


import java.util.ArrayList;

public class UnigramFixedSentence {
    public static ArrayList<String> UnigramResult;

    public UnigramFixedSentence(ArrayList<String> UnigramResult){
        this.UnigramResult = UnigramResult;
        BigramCorrection.print_flag = Boolean.FALSE;
    }

    public static String[] run(){
        StringBuilder stringBuilder = new StringBuilder();

        for (String s: UnigramResult){
            if (s.contains("[")){
                String[] tmp = s.split(" ");
                tmp[1] = tmp[1].replaceAll("\\[", "").replaceAll("\\]","");
                stringBuilder.append(tmp[1]+" ");
            } else if (!s.isEmpty()) {
                stringBuilder.append(s);
            }
        }

        System.out.print("\n\n========= Unigram fix and then Bigrams =========\n");
//        System.out.println("fixedResult: "+String.valueOf(stringBuilder));

        String[] fixedResultParts = String.valueOf(stringBuilder).split(" ");
        return fixedResultParts;
    }
}
