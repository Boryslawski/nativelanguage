package Util;

import Service.SjoService;
import Service.UniGramService;

import java.util.*;

public class UnigramCorrection {

    private static UniGramService uniGramService = new UniGramService();
    private static SjoService sjoService = new SjoService();
    private static ArrayList<String> corrected_text_unigram = new ArrayList<>();
    private static String most_propably_word = "";
    private String[] sentence_words;

    public UnigramCorrection(String[] sentence_words){
        this.sentence_words = sentence_words;
    }

    public ArrayList<String> run(){
        System.out.println("========= Unigram =========");

        for (String word : sentence_words){
            word = word.replaceAll("[-.,():\"\']","");
            Optional<String> opt = sjoService.getIfExist(word);

            if (!opt.isPresent()){
                HashMap<Integer, ArrayList<String>> similar_words = sjoService.getSimilarWords(word, 2);
                most_propably_word = unigram_probability(similar_words);
                corrected_text_unigram.add(most_propably_word+" ");
            }
            else {
                corrected_text_unigram.add(word+" ");
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        for(String s : sentence_words){
            stringBuilder.append(s+" ");
        }

        String cap = corrected_text_unigram.get(0).substring(0, 1).toUpperCase() + corrected_text_unigram.get(0).substring(1);
        corrected_text_unigram.set(0,cap);

        String last_word = corrected_text_unigram.get(corrected_text_unigram.size()-1).replaceAll("\\s+","");
        if (last_word.charAt(last_word.length()-1)!='.'){
            corrected_text_unigram.set(corrected_text_unigram.size()-1,last_word+".");
        }

        System.out.print("Poprawa: ");
        corrected_text_unigram.forEach(word -> System.out.print(word));
        return  corrected_text_unigram;
    }


    private static String unigram_probability(HashMap<Integer, ArrayList<String>> list){
        HashMap<Integer,String> unigrams = new HashMap<>();
        Map.Entry<Integer, ArrayList<String>> get_first_filter_level = list.entrySet().stream().iterator().next();
        ArrayList<String> words = list.get(get_first_filter_level.getKey());
        for (String word : words){
            int number = Integer.parseInt(uniGramService.getUnigram(word));
            unigrams.put(number, word);
        }

        Map<Integer, String> newMap = new TreeMap(Collections.reverseOrder());
        newMap.putAll(unigrams);
        if(!newMap.isEmpty()){
            Map.Entry<Integer,String> entry = newMap.entrySet().iterator().next();
            return entry.getValue();
        }else {
            return "";
        }
    }

    private static void printSimilarWords(HashMap<Integer, ArrayList<String>> list) {
        System.out.println("Proponowane wyrazy:");
        list.forEach((integer, strings) -> {
            System.out.println(" >> Poziom filtrowania (" + strings.size() + "): " + integer);
            strings.forEach(x -> System.out.println("   " + x));
        });
    }
}
