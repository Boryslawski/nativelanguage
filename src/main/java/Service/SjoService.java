package Service;

import Database.WordLocalDatabase;
import Util.LevenshteinAlgorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

public class SjoService {

    private final static WordLocalDatabase wordLocalDatabase = WordLocalDatabase.getInstance();
    private final LevenshteinAlgorithm levenshteinAlgorithm = new LevenshteinAlgorithm();
    private HashMap<Integer, ArrayList<String>> map;


    public Optional<String> getIfExist(String str){
        return wordLocalDatabase.getSjoFull().stream()
                .filter(x -> x.equalsIgnoreCase(str))
                .findAny();
    }

    public HashMap<Integer, ArrayList<String>> getSimilarWords(String word, int filerLevel){
        map = new HashMap<>();
        wordLocalDatabase.getSjoFull().forEach(s -> addToMap(levenshteinAlgorithm.levenshteinDistance(word, s), s));

        HashMap<Integer, ArrayList<String>> result = new HashMap<>();
        map.entrySet().stream()
                .filter(x -> x.getKey() <= filerLevel)
                .forEach(i -> result.put(i.getKey(), i.getValue()));

        return result;
    }

    private void addToMap(int key, String word){
        if(!map.containsKey(key)){
            map.put(key, new ArrayList<>());
            map.get(key).add(word);
        } else {
            map.get(key).add(word);
        }
    }
}
