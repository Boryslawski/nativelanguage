package Service;

import Database.Entity.UniGram;
import Database.WordLocalDatabase;
import Util.LevenshteinAlgorithm;

import java.util.ArrayList;
import java.util.HashMap;

public class UniGramService {

    private final WordLocalDatabase wordLocalDatabase = WordLocalDatabase.getInstance();
    private HashMap<Integer, ArrayList<String>> map;


    public String getUnigram(String word){

        // Get from UniGrams for number of occurrences for word //
        for (UniGram u : wordLocalDatabase.getUnigramsFull()){
            String firstWord = u.getFirstWord();

            // Word in Unigrams //
            if (firstWord.equals(word)){
                return u.getNumber();
            }

            // Word without special chars in Unigrams //
            firstWord = firstWord.replaceAll("[.,():\"\']","");
            if (firstWord.equals(word)){
                return u.getNumber();
            }
        }
        return "0";
    }
}
