package Service;

import Database.Entity.BiGram;
import Database.WordLocalDatabase;
import Util.LevenshteinAlgorithm;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class BiGramService {

    private final WordLocalDatabase wordLocalDatabase = WordLocalDatabase.getInstance();
    private final LevenshteinAlgorithm levenshteinAlgorithm = new LevenshteinAlgorithm();
    private HashMap<Integer, ArrayList<String>> map;

//    private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
//    private static EntityManager entityManager = entityManagerFactory.createEntityManager();


    public HashMap<Integer, ArrayList<String>> getSimilarBigrams(String word, int filerLevel){
        map = new HashMap<>();
        getBigramsPart().forEach(s -> addToMap(levenshteinAlgorithm.levenshteinDistance(
                word, s.getBigram()[1]+" "+ s.getBigram()[2]), s.getBigram()[0]+":"+s.getBigram()[1]+" "+ s.getBigram()[2]));

        HashMap<Integer, ArrayList<String>> result = new HashMap<>();
        map.entrySet().stream()
                .filter(x -> x.getKey() <= filerLevel)
                .forEach(i -> result.put(i.getKey(), i.getValue()));

        return result;
    }

    private void addToMap(int key, String word){
        if(!map.containsKey(key)){
            map.put(key, new ArrayList<>());
            map.get(key).add(word);
        } else {
            map.get(key).add(word);
        }
    }


    public ArrayList<BiGram> getBigramsPart(){
        return wordLocalDatabase.getBigramsPart();
    }
}


//    public static void main(String[] args) {
//
//        try {
//            InputStream is = new FileInputStream(new File("C:\\Users\\Julia\\Dropbox\\Study\\NativLanguageApp\\2grams"));
//            BufferedReader br = new BufferedReader(new InputStreamReader(is));
//
////            entityManager.getTransaction().begin();
//            String line;
//            int i = 0;
//            while ((line = br.readLine()) != null){
//                line = line.trim();
//                String[] parts = Arrays.stream(line.split(" ")).map(String::trim).toArray(String[]::new);
//                BiGram biGram = new BiGram(parts[0], parts[1], parts[2]);
//
//                entityManager.getTransaction().begin();
//                entityManager.persist(biGram);
//                entityManager.getTransaction().commit();
//
//
//            }
//
//            entityManager.close();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
