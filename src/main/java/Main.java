import Util.BigramCorrection;
import Util.FailSentenceCreator;
import Util.UnigramCorrection;
import Util.UnigramFixedSentence;

import java.awt.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.print("\n\n========= Zdanie wejściowe =========\nPodaj zdanie: ");
        Scanner user_input = new Scanner(System.in);
        String input_text = user_input.nextLine();
        String[] input_words = input_text.split(" ");
        System.out.println("\n========= Start =========\n");

//        String input_text = "Pani senatorze ja jade jutro do miasto po zakópy.";
//        String input_text = "Zapallam lapę która stoi na biórku.";
//        String input_text = "Stoję drzewo obok malego dom.";
//        String input_text = "Nasza tata jedzi do szkoła.";

//        // Unigram //
        UnigramCorrection unigramCorrection = new UnigramCorrection(input_words);
        ArrayList<String> unigramResult = unigramCorrection.run();

//        // Bigram //
        BigramCorrection bigramCorrection = new BigramCorrection(input_words);
        bigramCorrection.run();

        // Sentence Fixed by Unigram and than Bigrams //
//        UnigramFixedSentence unigramFixedSentence = new UnigramFixedSentence(unigramResult);
//        String[] fixedResult = unigramFixedSentence.run();
//
//        BigramCorrection bigramCorrection_2 = new BigramCorrection(fixedResult);
//        bigramCorrection_2.run();

        System.out.println("\n========= Finish =========\n");
    }

}
