package Database;

import Database.Entity.BiGram;
import Database.Entity.UniGram;
import lombok.Data;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@Data
public class WordLocalDatabase {

    private static WordLocalDatabase instance;

    private static ArrayList<String> sjoFull = new ArrayList<>();
    private static ArrayList<BiGram> bigramsPart = new ArrayList<>();
    private static ArrayList<UniGram> unigramsFull = new ArrayList<>();


    public static WordLocalDatabase getInstance() {

        if(instance == null) {

            instance = new WordLocalDatabase();
            fillLocalDbWithSjoFull();
            fillLocalDbWithBiGramsPart();
            fillLocalDbWithUniGramsFull();

//            System.out.println("SJO: " + sjoFull.size());
//            System.out.println("BiGrams: " + bigramsPart.size());
//            System.out.println("UniGrams: " + unigramsFull.size());
        }
        return instance;
    }

    private static void fillLocalDbWithSjoFull(){
        try {
            String path = System.getProperty("user.dir");
//            InputStream is = new FileInputStream(new File("C:\\Users\\Julia\\Dropbox\\Study\\NativLanguageApp\\wsadyDoList\\odm.txt"));
            InputStream is = new FileInputStream(new File(path+"\\odm.txt"));
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = br.readLine()) != null){
                String[] parts = Arrays.stream(line.split(",")).map(String::trim).toArray(String[]::new);
                Collections.addAll(sjoFull, parts);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void fillLocalDbWithBiGramsPart(){
        try {
            String path = System.getProperty("user.dir");
//            InputStream is = new FileInputStream(new File("C:\\Users\\Julia\\Dropbox\\Study\\NativLanguageApp\\wsadyDoList\\2grams"));
            InputStream is = new FileInputStream(new File(path+"\\2grams.txt"));
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = br.readLine()) != null){
                line = line.trim();
                String[] parts = Arrays.stream(line.split(" ")).map(String::trim).toArray(String[]::new);
                BiGram biGram = new BiGram(parts[0], parts[1], parts[2]);
                bigramsPart.add(biGram);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void fillLocalDbWithUniGramsFull(){
        try {
            String path = System.getProperty("user.dir");
//            InputStream is = new FileInputStream(new File("C:\\Users\\Julia\\Dropbox\\Study\\NativLanguageApp\\wsadyDoList\\1grams"));
            InputStream is = new FileInputStream(new File(path+"\\1grams.txt"));
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = br.readLine()) != null){
                line = line.trim();
                String[] parts = Arrays.stream(line.split(" ")).map(String::trim).toArray(String[]::new);

                UniGram uniGram = new UniGram(parts[0], parts[1]);
                unigramsFull.add(uniGram);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getSjoFull(){
        return sjoFull;
    }

    public ArrayList<BiGram> getBigramsPart(){
        return bigramsPart;
    }

    public ArrayList<UniGram> getUnigramsFull(){
        return unigramsFull;
    }
}
