package Database.Entity;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity
@Table(name = "BiGram")
public class BiGram implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String number;
    private String firstWord;
    private String secondWord;


    public BiGram(String number, String firstWord, String secondWord) {
        this.number = number;
        this.firstWord = firstWord;
        this.secondWord = secondWord;
    }

    public String[] getBigram(){
        String[] bigram = new String[3];
        bigram[0]=(this.number);
        bigram[1]=(this.firstWord);
        bigram[2]=(this.secondWord);
        return bigram;
    }
}
