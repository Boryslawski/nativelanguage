package Database.Entity;

import java.io.Serializable;

public class UniGram implements Serializable {

    private int id;
    private String number;
    private String firstWord;

    public UniGram(String number, String firstWord){
        this.number = number;
        this.firstWord = firstWord;
    }

    public String getNumber(){
        return this.number;
    }

    public String getFirstWord(){
        return this.firstWord;
    }

//    public String[] getUnigram(){
//        String[] bigram = new String[2];
//        bigram[0]=(this.number);
//        bigram[1]=(this.firstWord);
//        return bigram;
//    }

}