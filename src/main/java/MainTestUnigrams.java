import Util.FailWordCreator;

import java.util.ArrayList;

public class MainTestUnigrams {

    public static void main(String[] args) {

        FailWordCreator failWordCreator = new FailWordCreator();
        int percent = 70;

        ArrayList<String> sentences = new ArrayList<>();
        sentences.add("Panie senatorze ja jade jutro do miasta po zakupy.");
        sentences.add("Zapalam lampe która stoi na biurku.");
        sentences.add("Stoi drzewo obok malego dom.");
        sentences.add("Nasza tata jezdzi do szkoły.");


        for (String sentence : sentences) {
            System.out.println("\n\n========= +++++++++++++++++ =========");
            System.out.println("========= Zdanie wprowadzone =========");
            System.out.println("    "  + sentence);
            System.out.println("========= Zdanie przerobione na bledne =========");
            String failSentence = failWordCreator.create(sentence, 20);
            System.out.println("    "  + failSentence + "\n");


            String[] input_words = failSentence.split(" ");

////        // Unigram //
//            UnigramCorrection unigramCorrection = new UnigramCorrection(input_words);
//            ArrayList<String> unigramResult = unigramCorrection.run();
//
////        // Bigram //
//            BigramCorrection bigramCorrection = new BigramCorrection(input_words);
//            bigramCorrection.run();

            // Sentence Fixed by Unigram and than Bigrams //
//        UnigramFixedSentence unigramFixedSentence = new UnigramFixedSentence(unigramResult);
//        String[] fixedResult = unigramFixedSentence.run();
//
//        BigramCorrection bigramCorrection_2 = new BigramCorrection(fixedResult);
//        bigramCorrection_2.run();
        }
    }
}
